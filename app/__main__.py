from config import TG_BOT_TOKEN as TOKEN
from bot import create_bot

def main():

    bot = create_bot(TOKEN)
    bot.polling()

if __name__ == '__main__':
    main()