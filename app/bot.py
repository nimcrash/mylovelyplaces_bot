import telebot
from collections import defaultdict

COMMANDS = {
    'help': 'список комманд',
    'add': 'добавление нового места', 
    'list': 'отображение добавленных мест', 
    'reset': 'удаление всех добавленных локаций'
}

START, ADRESS, PHOTO, LOCATION = range(4)
USER_STATE = defaultdict(lambda: START)
USER_DATA = defaultdict(lambda : [])

def create_bot(token):

    bot = telebot.TeleBot(token)

    @bot.message_handler(commands=['start', 'help'])
    def start_handler(message):
        bot.send_message(chat_id=message.chat.id, text='Добро пожаловать!\nСписок доступных комманд:\n{}'.format(
            '\n'.join('/{} - {}'.format(key, value) for key, value in COMMANDS.items())))

    @bot.message_handler(commands=['add'], func=lambda message: get_state(message.from_user.id) == START)
    def add_handler(message):
        bot.send_message(chat_id=message.chat.id, text='Введи адрес нового места.')
        set_state(message.from_user.id, ADRESS)

    @bot.message_handler(func=lambda message: get_state(message.from_user.id) == ADRESS)
    def add_adress_handler(message):
        bot.send_message(chat_id=message.chat.id, text='Отправь фото.')
        set_user_data(message.from_user.id, 'adress', message.text, True)
        set_state(message.from_user.id, PHOTO)

    @bot.message_handler(func=lambda message: get_state(message.from_user.id) == PHOTO, content_types=['photo'])
    def add_photo_handler(message):
        bot.send_message(chat_id=message.chat.id, text='Отправь локацию.')
        set_user_data(message.from_user.id, 'photo', message.photo[-1].file_id)
        set_state(message.from_user.id, LOCATION)

    @bot.message_handler(func=lambda message: get_state(message.from_user.id) == LOCATION, content_types=['location'])
    def add_location_handler(message):
        bot.send_message(chat_id=message.chat.id, text='Готово. Информация о новом месте сохранена.')
        set_user_data(message.from_user.id, 'location', message.location)
        set_state(message.from_user.id, START)

    @bot.message_handler(commands=['list'], func=lambda message: get_state(message.from_user.id) == START)
    def list_of_places_handler(message):
        chat_id = message.chat.id
        number = 1
        for place in USER_DATA[message.from_user.id][-1:-10:-1]:
            bot.send_photo(chat_id, place['photo'], caption="{}. {}".format(number, place['adress']))
            bot.send_location(chat_id, latitude=place['location'].latitude, longitude=place['location'].latitude)
            number += 1        

    @bot.message_handler(commands=['reset'], func=lambda message: get_state(message.from_user.id) == START)
    def reset_handler(message):
        USER_DATA[message.from_user.id] = []
        bot.send_message(message.chat.id, text='Список мест очищен.')

    return bot

def set_state(user_id, state):
    USER_STATE[user_id] = state

def get_state(user_id):
    return USER_STATE[user_id]

def set_user_data(user_id, key, data, new_place=False):
    if new_place:
        USER_DATA[user_id].append({}) 

    USER_DATA[user_id][-1][key] = data

def get_user_data(user_id, key):
    return USER_DATA[user_id][-1][key]