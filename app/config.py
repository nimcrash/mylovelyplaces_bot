import os

TG_BOT_TOKEN = os.environ.get('TG_BOT_TOKEN')

# os.getenv is equivalent, and can also give a default value instead of `None`
#print(os.getenv('KEY_THAT_MIGHT_EXIST', default_value))
